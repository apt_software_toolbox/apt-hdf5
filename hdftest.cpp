#include <cstdlib>
#include <iostream>
#include <string>
#include <cassert>
#include <map>
#include <random>
#include <chrono>
#include <ctime>
#include <sstream>
#include <cstring>

using namespace std;

#include "common.h"

const char *ATOMPROBE_EXP_HEADER="AtomProbeTomographyExperiment";
const char *ATOMPROBE_HDF_VERSION="2020-01";

#ifdef DEBUG
//run unit tests
void runTests();
#endif

unsigned int createSimulatedAPTHDF5(const string &outFilename)
{
	std::random_device rd;
	std::mt19937 rGen(rd());
	std::uniform_real_distribution<> uGen(0.0,1.0);
	
	//Simulate uniform hits across detector
	const float DET_SIZE=0.04; //40mm
	const float IMAGING_FLIGHT_PATH=0.09; //90mm (Apparent distance of source, given projection on screen. Due to lenses, mirrors, may not be the same as actual flight path)
	const float TRAVERSED_FLIGHT_PATH=0.30; //300 mm; Actual traversed flight path for an ion travelling from the centre of an axis-aligned tip

	const unsigned int EXPERIMENT_SECONDS=10;
	const float EXPERIMENT_FREQ=1e5; //100 kHz

	//Open HDF5 for output
	H5::H5File file(outFilename,H5F_ACC_TRUNC);

	file.createGroup("/ExperimentContext");

	writeDataToGroup(file,"/ExperimentContext/SampleDescription",
			vector<string>({"Simulated Sample"}));
	
	writeDataToGroup(file,"/ExperimentContext/ExperimentType",
			vector<string>({ATOMPROBE_EXP_HEADER}));


	writeDataToGroup(file,"/ExperimentContext/Version",
			vector<string>({ATOMPROBE_HDF_VERSION}));

	//Sample name with unicode, and the ISO 639-1 2 letter code for language as test for 
	// script support
	writeDataToGroup(file,"/ExperimentContext/SampleName",
		vector<string>({"Simulated sample (unicode test: (Snowman ☃︎) (Ko:다) (Ja :ろ) (Zh : 視) (El: λ) (Th: บี) (Hi: ऋ ) (Ar: ط) (Ru : й)) (Pl:ą)"})); 


	writeDataToGroup(file,"/ExperimentContext/SampleUniqueIdentifier",
			vector<string>({"1-simulated"}));


	//write out aperture details
	writeDataToGroup(file,"/ExperimentContext/ApertureType/",vector<string>({"none"}));

	writeDataToGroup(file,"/ExperimentContext/ApertureUniqueIdentifier/",vector<string>({"none"}));
	
	file.createGroup("/ToolEnvironment");

	//Obtain time in timezone form 
	auto start= chrono::system_clock::now(); //C++ now object
	{
	time_t timev = chrono::system_clock::to_time_t(start); //Convert to time T
	struct tm *tmtime = gmtime(&timev); // Convert to UTC
	writeDataToGroup(file,"/ToolEnvironment/ExperimentStartDateUTC",
			vector<string>({gmToISO8601UTC(tmtime)}) ); //NOTE: This was originaly called ExperimentStartDateGlobal
	}

	//Simulate a 10 second experiment
	auto theNearFuture = start+ chrono::seconds(EXPERIMENT_SECONDS);
	{
	time_t timev = chrono::system_clock::to_time_t(theNearFuture); //Convert to time T
	struct tm *tmtime = gmtime(&timev); // Convert to UTC
	writeDataToGroup(file,"/ToolEnvironment/ExperimentEndDateUTC",
			vector<string>({gmToISO8601UTC(tmtime)}) );//NOTE: This was originaly called ExperimentEndDateGlobal
	}

	//Local timezone representation
	{
	time_t timev = chrono::system_clock::to_time_t(start); //Convert to time T
	struct tm *tmtime = localtime(&timev); // Convert to tm struct
	writeDataToGroup(file,"/ToolEnvironment/ExperimentStartDateLocal",
			vector<string>({tmToISO8601Local(tmtime)}) );//NOTE: This was originaly called ExperimentEndDateGlobal
	}
	
	file.createGroup("/ToolStateAndSettings");
	
	//----
	writeDataToGroup(file,"/ToolStateAndSettings/DetectorGeometryOpticalEquiv",
			vector<float>({0.4, 0.4}));

	//Possible entries are:
	// "DelayLine", "Camera", "WedgeStrip"
	writeDataToGroup(file,"/ToolStateAndSettings/DetectorType",
			vector<string>({"DelayLine"}));


	writeDataToGroup(file,"/ToolStateAndSettings/DetectorSize",
			vector<float>({DET_SIZE,DET_SIZE}));

	writeDataToGroup(file,"/ToolStateAndSettings/FlightPathSpatial",
			vector<float>({IMAGING_FLIGHT_PATH}));

	writeDataToGroup(file,"/ToolStateAndSettings/FlightPathTiming",
			vector<float>({TRAVERSED_FLIGHT_PATH}));
	
	writeDataToGroup(file,"/ToolStateAndSettings/InstrumentIdentifier",
			vector<string>({"SIM1"}));


	//Vector in tip coordinate space of incident laser beam, when active
	// 0 if not present
	writeDataToGroup(file,"/ToolStateAndSettings/LaserIncidence",
			vector<float>({-1,-1,0}));


	//Laser wavelength
	writeDataToGroup(file,"/ToolStateAndSettings/LaserWavelength",
			vector<float>{532e-9});

	//Possible entries are "Linear", "Spherical" and "None"
	writeDataToGroup(file,"/ToolStateAndSettings/ReflectronInfo",
			vector<string>({"None"}));

	writeDataToGroup(file,"/ToolStateAndSettings/DetectorReadout",
			vector<string>({"threshold"}));

	writeDataToGroup(file,"/ToolStateAndSettings/DetectorResolution",
			vector<float>({1e-5}));

	//Rotation + translation matrix that maps a coordinate system in 
	// physical tip space (defined with tip z=1 as the pointy end 
	// of the tip) to reconstruciton space.
	// Laboratory floor should always be || to v=1,0,0 in tip space.
	//  If system is still not constrained, then the right hand
	// direction as viewing instrument from the nominal front should
	// be x.

	// top-left 3x3 matrix is pure rotation.
	// right column, top 3 rows is x,y,z translation
	// bottom row must be 0,0,0,1, or 0,0,0,0
	// As is volume/orientation preserving det(M) = 1.

	//This form is the TRANSPOSE of the matrix, when written using writeDataToGroup
	const float data[] = { 1,0,0,0,
				0,1,0,0,
				0,0,1,0,
				0,0,0,1 };
	
	writeDataToGroup(file,"/ToolStateAndSettings/Tip2ReconSpaceMapping",
			data,4,4);
	


	//----

	file.createGroup("/ExperimentResults");

	const unsigned int NUMBER_PULSES=EXPERIMENT_FREQ*EXPERIMENT_SECONDS;

	//0,0,0 is defined to be the home scan position for the laser.
	//- positive Z is in the direction of the beam.
	//- Positive X is in the direction of the tip apex, if the tip
	// is perpendicular to the beam. 
	// Units are in metres.

	float *laserPos = new float[NUMBER_PULSES*3];
	memset(laserPos,0,sizeof(float)*NUMBER_PULSES*3);
	writeDataToGroup(file,"/ExperimentResults/LaserPosition",
			laserPos,NUMBER_PULSES,3,true);
	delete[] laserPos;

	{
	cout << "Simulating :" << NUMBER_PULSES << " pulses (not contiguous)" << endl;
	vector<float> laserPulseEnergy(NUMBER_PULSES,0.05e-9);
	writeDataToGroup(file,"/ExperimentResults/LaserEnergy",
			laserPulseEnergy,true);

	vector<float> pulseFrequency(NUMBER_PULSES,EXPERIMENT_FREQ);
	writeDataToGroup(file,"/ExperimentResults/PulseFrequency",
			pulseFrequency,true);

	vector<float> pulseVoltage(NUMBER_PULSES,1200);
	writeDataToGroup(file,"/ExperimentResults/PulseVoltage",
			pulseVoltage,true);

	vector<float> standVoltage(NUMBER_PULSES,8000);
	writeDataToGroup(file,"/ExperimentResults/StandingVoltage",
			standVoltage,true);
	
	vector<float> reflectVoltage(NUMBER_PULSES,8800);
	writeDataToGroup(file,"/ExperimentResults/ReflectronVoltage",
			reflectVoltage,true);
	
	}

	{
	//Sampled at 10Hz
	const unsigned int NUM_LOWRATE_SAMPLES=EXPERIMENT_SECONDS*10;
	cout << "Simulating :" << NUM_LOWRATE_SAMPLES<< " low-rate samples" << endl;

	vector<float> tipTemp(NUM_LOWRATE_SAMPLES,50);
	writeDataToGroup(file,"/ExperimentResults/TipTemperature",
			tipTemp,true);

	//0,0,0 is the home position of the stage. The coordinates
	// are in tip-coordinate space
	float *stagePos = new float[NUM_LOWRATE_SAMPLES*3];
	memset(stagePos,1,sizeof(float)*NUM_LOWRATE_SAMPLES*3);
	writeDataToGroup(file,"/ExperimentResults/StagePosition",
			stagePos,NUM_LOWRATE_SAMPLES,3,true);
	delete[] stagePos;
	}

	{
	vector<float> rawTof(NUMBER_PULSES,1000e-9);
	writeDataToGroup(file,"/ExperimentResults/TimeOfFlight",
			rawTof,true);

	float *hitPos = new float[NUMBER_PULSES*2];
	for(auto ui=0u;ui<NUMBER_PULSES;ui++)
	{
		hitPos[ui*2] = DET_SIZE*(uGen(rGen) - 0.5);
		hitPos[ui*2+1] = DET_SIZE*(uGen(rGen)-0.5);
	}
	writeDataToGroup(file,"/ExperimentResults/DetectorHitPositions",
			hitPos,NUMBER_PULSES,2);
	delete[] hitPos;


	const float DET_RATE=0.05;
	vector<uint64_t> pulseNum(NUMBER_PULSES);
	for(auto ui=0u;ui<pulseNum.size();ui++)
		pulseNum[ui] = ((float)ui/pulseNum.size())*(float)NUMBER_PULSES*1.0f/DET_RATE;
	
	writeDataToGroup(file,"/ExperimentResults/PulseNumber",
			pulseNum,true);
	}


	return 0;
}


void printUsage()
{
	cerr << "USAGE : program [--generate|--validate] APTH5_FILE" << endl;
}

int main(int argc, char *argv[])
{
#ifdef DEBUG
	if(argc == 2 && string(argv[1]) == string("--test"))
	{
		runTests();
		return 0;
	}
#endif

	if(argc!=3)
	{
		printUsage();
		return 1;
	}

	string filename;
	filename = argv[2];

	if(string(argv[1]) == "--validate")
	{
		const bool STRICT_MODE=true;
		auto errCode=validateAPTHDF5(filename,STRICT_MODE);

		if(errCode)
		{
			cerr << "Error validating file" << endl;
			return 1;
		}
		else
			cerr << "Validation checks passed" << endl;
	}
	else if(string(argv[1]) == "--generate")
	{

		auto errCode = createSimulatedAPTHDF5(filename);
		if(errCode)
			return 1;

	}
	else
	{
		printUsage();
		return 1;
	}



	return 0;
}


#ifdef DEBUG
void runTests()
{
	runCommonTests();
}
#endif
